<?php

namespace App;

use App\User;
use App\Http\Traits\TimestampTrait;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = ['author_id'];
    
    public function author()
    {
        return $this->belongsTo(\App\User::class, 'author_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public static function rules($id = '')
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:10000',
            'due_date' => 'required|date_format:Y-m-d\TH:i|after:'.(date('Y-m-d H:i')),
            'status' => 'integer|between:1,2',
            'user_id' => 'exists:App\User,id',
            

        ];
    }



    public function format(){

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'due_date' => dateFormat($this->due_date),
            'status' => $this->status,
            'created_at' => dateFormat($this->created_at),
            'user_id' => $this->user_id,
            'user_name' => $this->user->name,
            'user_email' => $this->user->email,
            'author_id' => $this->author_id,
            'author_name' => $this->author->name,
            'author_email' => $this->author->email,
        ];
    }
}
