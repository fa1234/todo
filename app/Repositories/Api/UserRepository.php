<?php
namespace App\Repositories\Api;

use Auth;
use App\User;
use Carbon\Carbon;
use App\Repositories\Api\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface{



      /**
     * @SWG\Get(
     *   path="/api/users",
     *   summary="Get All Users",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     * 	 @SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */
    public function getAll()
    {
        $users  = User::all();
        return response()->json($users, 200);
    }


     /**
     * @SWG\Get(
     *   path="/api/users/{id}",
     *   summary="Get user by id",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *  @SWG\Response(response=404, description="Not Found"),
     *		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="number"
     *      ),
     *   	@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */
    public function getById($id)
    {
        $user  = User::find($id);
        if (!$user) {
            return response()->json('Not found', 404);
        }
        return response()->json($user, 200);
    }






    /**
     * @SWG\Post(
     *   path="/api/signup",
     *   summary="Store new User",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     *		@SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     * 	@SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     * 	@SWG\Parameter(
     *          name="password_confirmation",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */

    public function signup($inputs)
    {
        

        $inputs->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = new User([
            'name' => $inputs->name,
            'email' => $inputs->email,
            'password' => bcrypt($inputs->password)
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }





    /**
     * @SWG\Put(
     *   path="/api/users",
     *   summary="Update logged in User info",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     *		@SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     * 	@SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     * 	@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */

    public function update($inputs)
    {
        $user_id = Auth::user()->id;
        $inputs->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users,email,'.$user_id,
            'password' => 'required|string'
        ]);

        $user = User::find($user_id);
        $user->name = $inputs->name;
        $user->email = $inputs->email;
        $user->password = bcrypt($inputs->password);
        $user->save();
        return response()->json($user, 201);
    }





    /**
     * @SWG\Delete(
     *   path="/api/users",
     *   summary="Delete logged in user",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),

     * 	@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */

    public function delete()
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        Auth::user()->token()->revoke();
        $user->delete();

        return response()->json([
            'message' => 'Successfully destroyed user!'
        ], 201);
    }
  
  



    /**
     * @SWG\Post(
     *   path="/api/login",
     *   summary="login to generate Bearer token",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     * 	@SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),


     * )
     *
     */
    public function login($inputs)
    {
        $inputs->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        $user = $inputs->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($inputs->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }



     /**
     * @SWG\Get(
     *   path="/api/logout",
     *   summary="logout (revoke Bearer Token)",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),


     * )
     *
     */



    public function logout()
    {
        Auth::user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

  

 /**
     * @SWG\Get(
     *   path="/api/user",
     *   summary="get logged in user",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),


     * )
     *
     */
    public function user()
    {
        return response()->json(Auth::user());
    }





}