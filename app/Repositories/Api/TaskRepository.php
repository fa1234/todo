<?php
namespace App\Repositories\Api;

use Auth;
use App\Task;
use App\Repositories\Api\TaskRepositoryInterface;

class TaskRepository implements TaskRepositoryInterface
{

        /**
     * @SWG\Get(
     *   path="/api/tasks",
     *   summary="Get All Tasks which are assigned to logged in user",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */

    public function getAllTasks()
    {
        $user_id = Auth::user()->id;
        $tasks = Task::where('user_id', $user_id)->orderBy('created_at', 'desc')
        ->with('author')->get()->map->format();
        return response()->json($tasks);
    }



    /**
     * @SWG\Get(
     *   path="/api/authoredTasks/",
     *   summary="Get All Tasks which are created by logged in user",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */
    public function getAuthoredTasks()
    {
        $user_id = Auth::user()->id;
        $tasks = Task::where('author_id', $user_id)->orderBy('created_at', 'desc')
        ->get()->map->format();
        return response()->json($tasks);
    }



    /**
     * @SWG\Get(
     *   path="/api/tasks/{id}",
     *   summary="Get task by id",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="string"
     *      ),
     * 		@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */

    public function getById($id)
    {

        $task = Task::find($id);


        if (!$task) {
            return response()->json(['message' => 'Record not found'], 404);
        }
        $user_id = Auth::user()->id;
       
        //check if task is assigned to logged in user or owns it
        if (($user_id == $task->user_id) || ($user_id == $task->author_id)) {
            return response()->json($task->format());
        } else {
            return response()->json(['message' => 'You don\'t have right to see current Task'], 403);
        }
    }

    /**
       * @SWG\Post(
       *   path="/api/tasks/",
       *   summary="Create new Task",
       *   operationId="testing",
       *   @SWG\Response(response=200, description="successful operation"),
       *   @SWG\Response(response=406, description="not acceptable"),
       *   @SWG\Response(response=500, description="internal server error"),
       *		@SWG\Parameter(
       *          name="name",
       *          in="formData",
       *          required=true,
       *          type="string"
       *      ),
       *		@SWG\Parameter(
       *          name="description",
       *          in="formData",
       *          required=true,
       *          type="string"
       *      ),
       * 	@SWG\Parameter(
       *          name="user_id",
       *          in="formData",
       *          required=true,
       *          type="number"
       *      ),
       * 	@SWG\Parameter(
       *          name="due_date",
       *          in="formData",
       *          required=true,
       *          type="string",
       *          format="date-time",
       *
       *      ),
       *	@SWG\Parameter(
       *          name="Authorization",
       *          in="header",
       *          required=true,
       *          type="string"
       *      ),
       * )
       *
       */


    public function create($inputs)
    {
        $inputs->validate(Task::rules());
        $object = new Task;
        $object->name = $inputs->name;
        $object->description = $inputs->description;
        $object->due_date = $inputs->due_date;
        $object->user_id = $inputs->user_id;
        $object->author_id = Auth::user()->id;
        $object->created_at = date('Y-m-d h:i:s');
        $object->save();

        return response()->json($object->format(), 201);
    }



    /**
     * @SWG\Put(
     *   path="/api/tasks/{id}",
     *   summary="Update existing Task by id",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="number"
     *      ),
     *		@SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     *		@SWG\Parameter(
     *          name="description",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     * 	@SWG\Parameter(
     *          name="user_id",
     *          in="formData",
     *          required=true,
     *          type="number"
     *      ),
     * 	@SWG\Parameter(
     *          name="due_date",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          format="date-time",
     *
     *      ),
     *	@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */

    public function update($inputs, $id)
    {
        $inputs->validate(Task::rules());
        $task = Task::find($id);
        if (!$task) {
            return response()->json(['message' => 'Record not found'], 404);
        }

        $user_id = Auth::user()->id;
        //check if editor is author of the task
        if ($user_id == $task->author_id) {
            $task->name = $inputs->name;
            $task->description = $inputs->description;
            $task->user_id = $inputs->user_id;
            $task->due_date = $inputs->due_date;
            $task->save();

            return response()->json($task->format(), 200);
        } else {
            return response()->json(['message' => 'You don\'t have right to update current Task'], 403);
        }
    }



    /**
       * @SWG\Delete(
       *   path="/api/tasks/{id}",
       *   summary="Delete task by id",
       *   operationId="testing",
       *   @SWG\Response(response=200, description="successful operation"),
       *   @SWG\Response(response=406, description="not acceptable"),
       *   @SWG\Response(response=500, description="internal server error"),
       *		@SWG\Parameter(
       *          name="id",
       *          in="path",
       *          required=true,
       *          type="number"
       *      ),
       *     	@SWG\Parameter(
       *          name="Authorization",
       *          in="header",
       *          required=true,
       *          type="string"
       *      ),
       * )
       *
       */



    public function delete($id)
    {
        $task = Task::find($id);

        if (!$task) {
            return response()->json('Task not found', 404);
        }
        //if user is author of the task
        if (Auth::user()->id == $task->author_id) {
            $task->delete();
            return response()->json('task Deleted successfully', 200);
        } else {
            return response()->json('You don\'t have right to Delete current Task', 403);
        }
    }




    /**
     * @SWG\Post(
     *   path="/api/changeTaskStatus/{id}",
     *   summary="Change Task status (pending (default) - 0, in progress - 1, done -2) can change author or assigned user ",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="string"
     *      ),
     *   	@SWG\Parameter(
     *          name="status",
     *          in="formData",
     *          required=true,
     *          type="integer"
     *      ),
     * 		@SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string"
     *      ),
     * )
     *
     */

    public function changeTaskStatus($id)
    {
        $task = Task::find($id);
        if (!$task) {
            return response()->json(['message' => 'Record not found'], 404);
        }
        if (request('status') != 2 && request('status') != 1) {
            return response()->json('Invalid data for status', 422);
        }
      
        $user_id = Auth::user()->id;
       
        //check if task is assigned to logged in user or owns it
        if (($user_id == $task->user_id) || ($user_id == $task->author_id)) {
            $task->status = request('status');
            $task->save();
            return response()->json($task->format());
        } else {
            return response()->json(['message' => 'You don\'t have right to see current Task'], 403);
        }
    }
}
