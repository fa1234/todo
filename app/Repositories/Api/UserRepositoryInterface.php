<?php
namespace App\Repositories\Api;

interface UserRepositoryInterface{

    
    public function getAll();

    public function getById($id);

    public function signup($id);

    public function update($inputs);
  
    public function delete();

    public function login($inputs);

    public function logout();

    public function user();
}