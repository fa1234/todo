<?php
namespace App\Repositories\Api;

interface TaskRepositoryInterface{

    
    public function getAllTasks();

    public function getAuthoredTasks();

    public function getById($id);

    public function create($inputs);

    public function update($inputs, $id);
  
    public function delete($id);

    public function changeTaskStatus($id);
}