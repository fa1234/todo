<?php
namespace App\Repositories;

use Auth;
use App\Task;
use App\User;
use App\Repositories\TaskRepositoryInterface;

class TaskRepository implements TaskRepositoryInterface
{

 

    public function getAllTasks()
    {
        $todos  = Auth::user()->todos()->orderBy('created_at', 'desc')->paginate(10);
        $users  = User::all();
        return view('tasks/index', compact('todos', 'users'));

    }




    public function getAuthoredTasks()
    {
        $todos  = Task::where('author_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(10);
        $users  = User::all();

        return view('tasks/authored', compact('todos', 'users'));
    }





    public function getById($id)
    {

        $task = Task::find($id);


        if (!$task) {
            return response()->json(['message' => 'Record not found'], 404);
        }
        $user_id = Auth::user()->id;
       
        //check if task is assigned to logged in user or owns it
        if (($user_id == $task->user_id) || ($user_id == $task->author_id)) {
            return response()->json($task->format());
        } else {
            return response()->json(['message' => 'You don\'t have right to see current Task'], 403);
        }
    }


    public function create()
    {
        $users  = User::all();
        return view('tasks/create', compact('users'));
    }

    public function store($inputs){
          
        $inputs->validate(Task::rules());
        $object = new Task();
        $object->name = $inputs->name;
        $object->description = $inputs->description;
        $object->user_id = $inputs->user_id;
        $object->author_id = Auth::user()->id;
        $object->due_date = $inputs->due_date;
        $object->save();

        return redirect(route('tasks.authored'))->with('success', 'Todo added Successfully');
    }


    public function edit($id){
        $task = Task::find($id);
        $users  = User::all();
        if (!$task) {
            abort(404);
        }

        if (Auth::user()->id == $task->author_id) {
            return view('tasks/edit', compact('task', 'users'));
        } else {
            return redirect(route('tasks.authored'))->with('error', 'You don\'t have right to edit that Task');
        }
    }


    

    public function update($inputs, $id)
    {
        //check if user has right to update task

        $inputs->validate(Task::rules());
       

        $task = Task::find($id);
        if (Auth::user()->id == $task->author_id) {
            $task->name =$inputs->name;
            $task->description = $inputs->description;
            $task->user_id = $inputs->user_id;
            $task->due_date = $inputs->due_date;
            $task->save();

            return redirect(route('tasks.authored'))->with('success', 'task updated successfully');
        } else {
            return redirect(route('tasks.authored'))->with('error', 'You don\'t have right to edit Task');
        }
    }




    public function delete($id)
    {
        $task = Task::find($id);

        if (!$task) {
            abort(404);
        }

        if (Auth::user()->id == $task->author_id) {
            $task->delete();

            return redirect(route('tasks.authored'))->with('success', 'task Deleted successfully');
        } else {
            return redirect(route('tasks.authored'))->with('error', 'You don\'t have right to Delete Task');
        }
    }





    public function statusUpdate($id)
    {
        $task = Task::find($id);
        if (!$task) {
            abort(404);
        }

        if(Auth::user()->id == $task->user_id || Auth::user()->id == $task->author_id){
            $task->status = request('status');
            $task->save();

            return redirect()->back()->with('success', 'Status changed successfully');
        }else{
            return redirect()->back()->with('error', 'Something Went Wrong');
        }


    }
}
