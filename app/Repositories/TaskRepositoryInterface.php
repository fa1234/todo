<?php
namespace App\Repositories;

interface TaskRepositoryInterface{




    public function getAllTasks();

    public function getAuthoredTasks();

    public function getById($id);

    public function create();

    public function store($inputs);

    public function edit($id);    

    public function update($inputs, $id);
  
    public function delete($id);

    public function statusUpdate($id);
}