<?php


 function dateFormat($date)
 {
     return  \Carbon\Carbon::parse($date)->diffForHumans(['parts', 3]);
 }
