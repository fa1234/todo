<?php

namespace App\Providers;


use App\Repositories\TaskRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Api\UserRepository;
use App\Repositories\TaskRepositoryInterface;
use App\Repositories\Api\UserRepositoryInterface;
use App\Repositories\Api\TaskRepository as ApiTaskRepository;
use App\Repositories\Api\TaskRepositoryInterface as ApiTaskRepositoryInterface;


class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(TaskRepositoryInterface::class, TaskRepository::class);
        $this->app->bind(ApiTaskRepositoryInterface::class, ApiTaskRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);


    }
}
