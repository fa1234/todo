<?php
namespace App\Http\Traits;

use Carbon\Carbon;

trait TimestampTrait
{
    public $timestamps = true;

    public function getDates()
    {
        return ['created_at', 'updated_at'];
    }

    public function getCreatedAtAttributes()
    {
        die(print_r($this->attribute['created_at']));
        return Carbon::parse($this->attribute['created_at']->diffForHumans());
    }

    public function getUpdatedAtAttributes()
    {
        return Carbon::parse($this->attribute['updated_at']->diffForHumans());
    }
}
