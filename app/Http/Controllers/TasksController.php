<?php

namespace App\Http\Controllers;

use Auth;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use App\Repositories\TaskRepository;
use App\Repositories\TaskRepositoryInterface;

class TasksController extends Controller
{

    private $taskRepository;
    public function __construct(TaskRepositoryInterface $taskRepository){
        $this->taskRepository = $taskRepository;
    }

    //get tasks which are assigned to signed in user
    public function index()
    {
        return $this->taskRepository->getAllTasks();
    }


    //get tasks which created signed in user
    public function authored()
    {
        return $this->taskRepository->getAuthoredTasks();
    }

    //return crete view with users list
    public function create()
    {

        return $this->taskRepository->create();
    }


    //save new task
    public function store(Request $request)
    {

        return $this->taskRepository->store($request);
  
    }


    //return edit view with current task and users list (check if logged in user owns current task)
    public function edit($id)
    {
        return $this->taskRepository->edit($id);
    }

    
    //update current task (check if logged in user owns current task)
    public function update(Request $request, $id)
    {
        return $this->taskRepository->update($request, $id);
    }

    //delete current task (check if logged in user owns current task)
    public function destroy($id)
    {
        return $this->taskRepository->delete($id);
    }

    //change task status (check if logged in user owns current task or is assigned to that task)
    public function statusUpdate($id)
    {
       
        return $this->taskRepository->statusUpdate($id);

    }
}
