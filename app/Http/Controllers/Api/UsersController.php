<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Api\UserRepositoryInterface;

class UsersController extends Controller
{

    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository){

        $this->userRepository = $userRepository;

    }

    public function index()
    {
        return $this->userRepository->getAll();
    }



    public function show($id)
    {
        return $this->userRepository->getById($id);
    }

    public function signup(Request $request)
    {
        return $this->userRepository->signup($request);
    }

    public function update(Request $request)
    {
        return $this->userRepository->update($request);
    }

    public function destroy()
    {
        return $this->userRepository->delete();
    }

    public function login(Request $request)
    {
        return $this->userRepository->login($request);
    }

    public function logout()
    {
        return $this->userRepository->logout();
    }

    public function user()
    {
        return $this->userRepository->user();
    }
}
