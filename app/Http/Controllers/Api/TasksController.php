<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Api\TaskRepositoryInterface;


class TasksController extends Controller
{
    private $taskRepository;
    
    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository  = $taskRepository;
    }





    //get all tasks which are assigned to logged in user
    public function index()
    {
        return $this->taskRepository->getAllTasks();
    }


    
    //show single task (if logged in user is  author or assigned user )
    public function show($id)
    {
        return $this->taskRepository->getById($id);
    }

    //if logged in create new task
    public function store(Request $request)
    {
        return $this->taskRepository->create($request);
    }

    //if logged in user is author of the specific task: update
    public function update(Request $request, $id)
    {
        return $this->taskRepository->update($request, $id);
    }

    //if logged in user is author of the specific task:  delete 
    public function destroy($id)
    {
        return $this->taskRepository->delete($id);
    }

    //get all tasks which created logged in user
    public function authoredTasks()
    {
        return $this->taskRepository->getAuthoredTasks();
    }


    //change task status (in progress 1, done - 2), if logged in user is author or assigned
    public function changeTaskStatus($id)
    {
        return $this->taskRepository->changeTaskStatus($id);
    }
}
