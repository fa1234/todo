<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


//users
Route::post('login', 'Api\UsersController@login');
Route::post('signup', 'Api\UsersController@signup');


Route::middleware('auth:api')->group(function () {
    //Users
    Route::get('logout', 'Api\UsersController@logout');
    Route::get('user', 'Api\UsersController@user');

    Route::get('users', 'Api\UsersController@index');
    Route::put('users', 'Api\UsersController@update');
    Route::delete('users', 'Api\UsersController@destroy');

    Route::get('users/{id}', 'Api\UsersController@show');

    //tasks
    Route::get('tasks', 'Api\TasksController@index');
    Route::get('authoredTasks', 'Api\TasksController@authoredTasks');

    Route::post('changeTaskStatus/{id}', 'Api\TasksController@changeTaskStatus');



    
    Route::get('tasks/{id}', 'Api\TasksController@show');
    Route::post('tasks', 'Api\TasksController@store');
    Route::put('tasks/{id}', 'Api\TasksController@update');

    Route::delete('tasks/{id}', 'Api\TasksController@destroy');
});
