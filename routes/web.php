<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return redirect('/tasks');
});

Auth::routes();


Route::get('/users', function(){
    return \App\User::all();
});



Route::middleware(['auth'])->group(function () {
    Route::get('/authored', 'TasksController@authored')->name('tasks.authored');

    Route::resource('tasks', 'TasksController');
    Route::put('/userUpdate/{id}', 'TasksController@statusUpdate')->name('tasks.userUpdate');
});
