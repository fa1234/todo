@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>ToDo List</h1>

            @if(session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
            @elseif(session('error'))
            <div class="alert alert-success">{{session('error')}}</div>

            @endif

            <form method="POST" action="{{ route('tasks.store') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Description</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Users</label>

                    <div class="col-md-6">
                        <select name="user_id" id="" class="form-control">
                            @foreach ($users as $user)
                            <option value="{{$user->id}}" {{Auth::user()->id == $user->id ?'selected' : ''}} >
                                {{$user->name}}
                                {{Auth::user()->id == $user->id ?'(Me)' : ''}} 
                            </option>
                                
                            @endforeach
                        </select>

                        @error('user_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Due Date</label>

                    <div class="col-md-6">
                    <input id="due_date" type="datetime-local" min="{{date('Y-m-d\TH:i')}}" class="form-control @error('name') is-invalid @enderror" name="due_date" value="{{ old('due_date') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

            

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Add Todo
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>
    <hr>
    @if(count($todos) > 0)

    @foreach ($todos as $todo)

    @php
        if($todo->status == 0){
            $class = 'alert-warning';
            $text = 'Pending';

        }else if($todo->status == 1){
            $class = 'alert-info';
            $text = 'In progress';
        }else if($todo->status == 2){
            $class = 'alert-success';
            $text = 'Done';
        };
    @endphp
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">
             Name:   {{$todo->name}}
            <div style="float: right" class="alert  {{$class}}">Status: {{$text}}</div>

            <div style="float: right">Assigned By: {{Auth::user()->id == $todo->author->id ?' Me' : $todo->author->name}}</div>

            </div>


                



                <div class="card-body">
               


                Description:    {{$todo->description}}

                <div style="float:right">
                Change task Status:
                <form method="POST" action="{{ route('tasks.userUpdate', $todo->id )}}">
                    @method('put')
                    @csrf

                    <select name="status" id="" class="form-control">
                        <option value="0" {{$todo->status == 0 ? 'selected' : ''}} disabled>pending</option>
                        <option value="1" {{$todo->status == 1 ? 'selected' : ''}}>In proggress</option>
                        <option value="2" {{$todo->status == 2 ? 'selected' : ''}}>Done</option>
                    </select>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Change
                            </button>
                        </div>
                    </div>
                </form>
         
                </div>
                </div>

                <div class="card-footer">
                    
                <p>Assigned: {{$todo->created_at->diffForHumans(null, false, true)}}</p>
                <p>Time left:{{ \Carbon\Carbon::parse($todo->due_date)->diffForHumans(['parts' => 3]) }}</p>
                    @if($todo->author_id == Auth::user()->id)
                <div style="floate:right">
                    <a href="#" class="btn btn-info">Edit Todo</a>
                </div>
                @endif
                {{-- <p>Assigned: {{$todo->due_date}}</p> --}}


       
                   </div>
            </div>
        </div>
    </div>

    <br>
    <br>
    @endforeach

    @else

    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>No Todos</h1>
        </div>
    </div>

    @endif
</div>
@endsection
