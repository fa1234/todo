@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
                @elseif(session('error'))
                <div class="alert alert-success">{{session('error')}}</div>
                @endif
            <div class="card">
                <div class="card-header">Add Task</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('tasks.store') }}">
                        @csrf
        
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
        
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
        
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                      
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
        
                            <div class="col-md-6">
                                {{-- <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="description" autofocus> --}}

                                <textarea name="description" id="description" cols="30" rows="10" class="form-control  @error('description') is-invalid @enderror" required ></textarea>
        
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">Users</label>
        
                            <div class="col-md-6">
                                <select name="user_id" id="" class="form-control @error('user_id') is-invalid @enderror ">
                                    @foreach ($users as $user)
                                    <option value="{{$user->id}}" {{Auth::user()->id == $user->id ?'selected' : ''}} >
                                        {{$user->name}}
                                        {{Auth::user()->id == $user->id ?'(Me)' : ''}} 
                                    </option>
                                        
                                    @endforeach
                                </select>
        
                                @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Due Date</label>
        
                            <div class="col-md-6">
                            <input id="due_date" type="datetime-local" min="{{date('Y-m-d\TH:i')}}" class="form-control @error('due_date') is-invalid @enderror" name="due_date" value="{{ old('due_date') }}" required autocomplete="name" autofocus>
        
                                @error('due_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
                    
        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Add Todo
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

