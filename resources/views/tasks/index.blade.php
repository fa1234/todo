@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>ToDo List</h1>

            @if(session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
            @elseif(session('error'))
        <div class="alert alert-danger">{{session('error')}}</div>

            @endif
        </div>
    </div>

    <br>
    <hr>
    @if(count($todos) > 0)

    @foreach ($todos as $todo)

    @php
        if($todo->status == 0){
            $class = 'alert-warning';
            $text = 'Pending';

        }else if($todo->status == 1){
            $class = 'alert-info';
            $text = 'In progress';
        }else if($todo->status == 2){
            $class = 'alert-success';
            $text = 'Done';
        };
    @endphp
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">
                <div class="left-side" style="width: 74%; display:inline-block">
                    Task Name:   {{$todo->name}}
                    <div style="">Assigned By: {{Auth::user()->id == $todo->author->id ?' Me' : $todo->author->name}}</div>
                </div>
             <div style="width:25%; display:inline-block; margin:0; padding:5px 10px" class="alert  {{$class}}">Status: {{$text}}</div>
            </div>

                <div class="card-body">
                    <div class="left-side" style="width: 74%; display:inline-block">
                     {{$todo->description}}
                    </div>  

                <div style="float:right">
            
                </div>
                </div>

                <div class="card-footer">
                <div class="left-side" style="width: 50%; display:inline-block">
                    <p>Assigned: {{$todo->created_at->diffForHumans(null, false, true)}}</p>
                    <p>Time left:{{ \Carbon\Carbon::parse($todo->due_date)->diffForHumans(['parts' => 3]) }}</p>
                    {{-- <p>Standart Format:{{ date('Y-m-d g:ia', strtotime($todo->due_date)) }}</p> --}}

                      

                </div>

                <div class="left-side col-6" >
                Change task Status:
                <form method="POST" action="{{ route('tasks.userUpdate', $todo->id )}}">
                    @method('put')
                    @csrf

                    <select name="status" id="" class="form-control col-4" style="display: inline-block">
                        <option value="0" {{$todo->status == 0 ? 'selected' : ''}} disabled>pending</option>
                        <option value="1" {{$todo->status == 1 ? 'selected' : ''}}>In proggress</option>
                        <option value="2" {{$todo->status == 2 ? 'selected' : ''}}>Done</option>
                    </select>

                    
                    <div class="col-md-6" style="display: inline-block">
                        <button type="submit" class="btn btn-primary">
                            Change
                        </button>
                    </div>

                </form>
                </div>
                <hr>
                @if($todo->author_id == Auth::user()->id)

                <div class="left-side" style="float:right">
                <form action="{{route('tasks.destroy', $todo->id)}}" method="post">
                @method('delete')
                @csrf
                <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?')" value="Delete">
                </form>
                </div>


                    <div style="float:left">
                        <a href="{{route('tasks.edit', $todo->id)}}" class="btn btn-info">Edit Todo</a>
                        </div>
                @endif



       
                   </div>
            </div>
        </div>
    </div>

    <br>
    <br>
    @endforeach
    {{ $todos->links() }}
    @else

    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>No Todos</h1>
        </div>
    </div>

    @endif
</div>
@endsection

